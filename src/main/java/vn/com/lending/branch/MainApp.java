package vn.com.lending.branch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.ApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
public class MainApp {
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(MainApp.class);		
	}
}
