package vn.com.lending.branch.config;

import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties(prefix = "history")
@Getter
@Setter
public class AppConfig {

    private List<String> transferServicesView;

    private String vpbankViewName = "VPBank";

    private int maxOldHistoryDays = 90;

    private int maxPageSize = 100;
}
