package vn.com.lending.branch.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import vn.com.lending.publics.branch.event.CreateBranchEvent;

@Configuration
public class KafkaProducerConfig {

	@Autowired
	private KafkaProducerProperties kafkaProducerProperties;

	@Bean
	public ProducerFactory<String, CreateBranchEvent> producerFactory() {
		return new DefaultKafkaProducerFactory<>(producerConfigs(), stringKeySerializer(), eventJsonSerializer());
	}

	@Bean
	public Map<String, Object> producerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProducerProperties.getBootstrap_servers());
		return props;
	}

	@Bean
	public KafkaTemplate<String, CreateBranchEvent> eventsKafkaTemplate() {
		KafkaTemplate<String, CreateBranchEvent> kafkaTemplate = new KafkaTemplate<>(producerFactory());
		kafkaTemplate.setDefaultTopic(kafkaProducerProperties.getTopic());
		return kafkaTemplate;
	}

	@Bean
	public Serializer stringKeySerializer() {
		return new StringSerializer();
	}

	@Bean
	public Serializer eventJsonSerializer() {
		return new JsonSerializer();
	}
}
