package vn.com.lending.branch.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
@Getter
@Setter
public class YAMLConfig {
	@Value("${kafka.bootstrap_servers}")
	private String bootstrap_servers;
	
	@Value("${kafka.group-id}")	
	private String group_id;
	
	@Value("${kafka.enable_auto_commit}")
	private Boolean enable_auto_commit;
	
	@Value("${kafka.auto_offset_reset}")
	private String auto_offset_reset;
	
	@Value("${kafka.auto_commit_interval_ms}")
	private int auto_commit_interval_ms;
	
	@Value("${kafka.session_timeout_ms}")
	private int session_timeout_ms;
	
	@Value("${kafka.key_deserializer}")
	private String key_deserializer;
	
}
