package vn.com.lending.branch.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "kafka")
public class KafkaProducerProperties {

    private String bootstrap_servers;
    private String topic;
   
}
