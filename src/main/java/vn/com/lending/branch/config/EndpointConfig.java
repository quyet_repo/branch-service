package vn.com.lending.branch.config;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.Getter;
import lombok.Setter;

/**
 * @author <a href="mailto:thanhnc6@vpbank.com.vn">thanhnc6</a> Created Apr 27,
 *         2020
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "endpoint")
public class EndpointConfig {

	private int msTimeoutMs = 20 * 1000;

	private int iapiTimeoutMs = 20 * 1000;

	private String internalApiUrl = "http://10.37.16.126:7808";

	private String registerSMSBankingURI = "/iapi/msaiib";
	
	private String gatewaySendPushAppURI = "http://10.37.16.96:8081/ottnotice/vpb/sendMultiMessages";

	@Bean
	public WebClient internalApiUrlClient() {
		return WebClient.builder().baseUrl(getInternalApiUrl()).build();
	}
}
