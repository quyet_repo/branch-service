package vn.com.lending.branch.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "branch")
public class Branch {
	@Id
	private int id;
	@Column(name="branch_id")
	private String branchId;
	private String name;
	@Column(name = "phone_number")
	private String phoneNumber;
	private String address;
	private String district;
	private String province;
	private String representative;
	private Integer status;
	@Column(name = "create_time")
	private Timestamp createTime;
	@Column(name = "update_time")
	private Timestamp updateTime;
	@Column(name = "user_created")
	private String userCreated;
	@Column(name = "user_updated")
	private String userUpdated;
}
