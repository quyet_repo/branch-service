package vn.com.lending.branch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import vn.com.lending.branch.model.Branch;

public interface BranchRepository extends JpaRepository<Branch, String>{
	@Query(value = "select u from Branch u where u.name = :name or u.phoneNumber =:phoneNumber")
	List<Branch> findByName(@Param("name") String accountNumber,@Param("phoneNumber") String phoneNumber);
}
