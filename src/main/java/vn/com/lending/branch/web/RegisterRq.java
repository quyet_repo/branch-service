package vn.com.lending.branch.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class RegisterRq {
	private String name;	
	private String phoneNumber;
	private String province;
	private String district;
	private String address;
	private String representative;
	private Integer status;
}
