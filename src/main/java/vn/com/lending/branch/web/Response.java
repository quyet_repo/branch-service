package vn.com.lending.branch.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
	private String status;
	private String mess;
	private String code;
	
	public Response(String status,String mess) {
		this.status = status;
		this.mess = mess;
	}
}
