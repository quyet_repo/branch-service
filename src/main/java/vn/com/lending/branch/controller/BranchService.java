package vn.com.lending.branch.controller;

import java.sql.Timestamp;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import vn.com.lending.branch.config.KafkaProducerProperties;
import vn.com.lending.branch.model.Branch;
import vn.com.lending.branch.repository.BranchRepository;
import vn.com.lending.branch.web.RegisterRq;
import vn.com.lending.branch.web.Response;
import vn.com.lending.publics.branch.event.CreateBranchEvent;

@Slf4j
@Service
public class BranchService {
	@Autowired
	private KafkaProducerProperties kafkaProducerProperties;
	@Autowired
	private KafkaTemplate<String, CreateBranchEvent> kafkaTemplate;
	@Autowired
	private BranchRepository branchRepository;
	@Autowired
	private BranchService branchService;
	
	public Response sendMessage(RegisterRq request ) {
		log.debug("Create branch request: [{}]",request);
		CreateBranchEvent message = convert(request);
		kafkaTemplate.setDefaultTopic("vn.com.lending.branch.event");
		ListenableFuture<SendResult<String, CreateBranchEvent>> future = kafkaTemplate
				.sendDefault(message.getBranchId(), message);
		future.addCallback(new ListenableFutureCallback<SendResult<String, CreateBranchEvent>>() {
			@Override
			public void onSuccess(SendResult<String, CreateBranchEvent> result) {
				//save
				Branch branch = new Branch();
				branch.setBranchId(message.getBranchId());
				branch.setName(message.getName());
				branch.setPhoneNumber(message.getPhoneNumber());
				branch.setProvince(message.getProvince());
				branch.setDistrict(message.getDistrict());
				branch.setAddress(message.getAddress());
				branch.setRepresentative(message.getRepresentative());
				branch.setStatus(1);		
				branch.setCreateTime(message.getCreateTime());
				branch.setUpdateTime(message.getUpdateTime());
				branch.setUserCreated(message.getUserCreated());
				branch.setUserUpdated(message.getUserUpdated());
				branchRepository.save(branch);
			}
			@Override
			public void onFailure(Throwable ex) {
				
			}
		});
		Response response = new Response("00","Success",message.getBranchId());
		return response;
	}
	
	public CreateBranchEvent convert(RegisterRq request) {
		CreateBranchEvent message = new CreateBranchEvent();
		String requestId = UUID.randomUUID().toString();
		message.setName(request.getName());
		message.setBranchId(requestId);
		message.setPhoneNumber(request.getPhoneNumber());
		message.setProvince(request.getProvince());
		message.setDistrict(request.getDistrict());
		message.setAddress(request.getAddress());
		message.setRepresentative(request.getRepresentative());
		message.setStatus(1);		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		message.setCreateTime(timestamp);
		message.setUpdateTime(timestamp);
		message.setUserCreated("quyetpv");
		message.setUserUpdated("quyetpv");
		return message;
	}
}
