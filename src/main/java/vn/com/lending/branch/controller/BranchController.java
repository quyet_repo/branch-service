package vn.com.lending.branch.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import vn.com.lending.branch.model.Branch;
import vn.com.lending.branch.repository.BranchRepository;
import vn.com.lending.branch.web.ErrorRs;
import vn.com.lending.branch.web.RegisterRq;
import vn.com.lending.branch.web.Response;
import vn.com.lending.publics.branch.event.CreateBranchEvent;

@Slf4j
@RestController
public class BranchController {
	@Autowired
	private BranchRepository branchRepository;
	@Autowired
	private BranchService branchService;
	@Autowired
	private KafkaTemplate<String, CreateBranchEvent> kafkaTemplate;

	@PostMapping(value = "/register")
	@ApiResponses(value = { @ApiResponse(code = 404, message = "Not Found Exception", response = ErrorRs.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorRs.class) })
	public Response register(@RequestBody RegisterRq registerRq) {
		List<Branch> lst = branchRepository.findByName(registerRq.getName(), registerRq.getPhoneNumber());
		if (lst.size() > 0) {
			return new Response("01", "name or phone does not exist");
		} else {
			return branchService.sendMessage(registerRq);
		}

	}
	@GetMapping(value = "/search")
	public List<Branch> search() {
		return branchRepository.findAll();
	}

}
